# About the course

Course has three modules. The first contains all about graphs. 
The second contains tasks and algorithms under the common name "Dynamic programming".
The third part makes inroduction into some algebraic algorithms.

❗️ Our lectures will be in the **230, Main Building.** Every week, it starts in **3.30 p. m.**

Form with the feedback for the contests and code review: [link](https://forms.gle/sAXHznsPqiNtHeEN7).

## Useful links

Marks you can find [here](https://docs.google.com/spreadsheets/d/1wVWFfQbaKPpcfpnjwHtE8LFGEFiAxS1CDHOpng94HfA/edit?usp=sharing).

Full playlist with recordings is [here](https://www.youtube.com/playlist?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N).

| Name | Lecture | Recording | Contest |
| ------ | ------ | ------ | ------ |
| Graphs and BFS | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L1%20-%20GRAPHS%20AND%20BFS.pdf) | [link](https://youtu.be/Fh8Zx-aT3HA?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/29156/enter/) |
| DFS and F.-F. Algo | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L2%20-%20DFS%20AND%20FORD-FULKERSON%20ALGORITHM.pdf) | [link](https://youtu.be/wGrgsRwNiXc?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/29335/enter/) |
| Shortest Paths | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L3%20-%20THE%20SHORTEST%20PATHS.pdf) | [link](https://youtu.be/uhYJkniBjmM?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/29640/enter/) |
| MST | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L4%20-%20MINIMUM%20SPANNING%20TREE.pdf) | [link](https://youtu.be/t61PbaF0yQs?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/30079/enter) |
| DP-1 | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L5%20-%20DYNAMIC%20PROGRAMMING.%20PART%201.pdf) | [link](https://youtu.be/I-b6HSnLppQ?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/30962/enter/) |
| DP-2 | [link](https://gitlab.com/DMCKG/algorithms-and-data-structures-ii-2021/-/blob/master/lectures/L6%20-%20DYNAMIC%20PROGRAMMING.%20PART%202.pdf) | [link](https://youtu.be/i4TwICEC_Yc?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) |[link](https://contest.yandex.ru/contest/30962/enter/) |
| DFA-1 | link | [link](https://youtu.be/S4dLrpKNexk?list=PLthfp5exSWEpMfl2S7kpL3fe94bx0Ji4N) | link |

## Program

### Graph's algorithms and structures
- Graphs. Breadth-first search. The shortest path between two nodes. Finding connected components.
- Depth-first search. Finding strongly connected components. Topological sort. Ford-Fulkerson algortihm.
- Weighted graphs. Dijkstra's algorithm. Bellman-Ford algorithm.
- Minimum spanning tree. Boruvka's algorithm. Prim's algorithm. Kruskal's algorithm.

### Dynamic Programming and DFA
- Dynamic Programming, Part-1. Fibonacci sequence. Memorization. Longest increasing subsequence. DP in the graphs.
- Dynamic Programming, Part-2. Edit distance. Finding palindromes. Matrix chain multiplication. Knapsack problem.
- Deterministic finite automaton, regular languages. Knuth-Morris-Pratt algorithm.
- Non-deterministic finite automaton. Pumping lemma. Aho–Corasick algorithm. Context-free languages.

### Others
- Cartesian tree. Operations: split, merge
- Introduction in modular arithmecity. Fermat and Miller–Rabin primality tests.
- P, EXP, R, NP classes. Reduction. NP-completeness.

## Grade System

**\+ 7 points** for all contests

**\+ 4 points** for writing tests after modules

**\+ 1 point** for last verbal test

**\+ 1 point** for the final contest

## References
1. [J. A. Bondy, U.S.R. Murty - Graph theory - 2008](https://drive.google.com/file/d/1MiyG5iPXEMth2PZu3pa6Ck5TmGN91SaO/view?usp=sharing)
2. [R. Sedgewick, K. Wayne - Algorithms. Fourth Edition - 2011](https://algs4.cs.princeton.edu/home/)
3. [MIT Open Courses - Introduction to Algorithms - 2011](https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/)
4. [Donald E. Knuth - The Art Of Computer Programming. Vol. 1 Fundamental Algorithms - 1969]()
5. [T. H. Cormen, C. E. Leiserson, R. L. Rivest, C. Stein - Introduction to algorithms. Third edition - 2009](https://sd.blackball.lv/library/Introduction_to_Algorithms_Third_Edition_(2009).pdf)
